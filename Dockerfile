FROM webdevops/php-nginx:8.2

RUN pecl install pcov
RUN docker-php-ext-enable pcov


ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils libpq-dev

RUN docker-php-ext-install bcmath pdo pdo_pgsql

RUN apt-get install -y less wget procps screen sudo

RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

RUN echo "client_max_body_size 50M;" > /opt/docker/etc/nginx/vhost.common.d/10-general.conf
RUN echo "gzip on;" >> /opt/docker/etc/nginx/vhost.common.d/10-general.conf
RUN echo "gzip_types      text/plain application/xml application/json;" >> /opt/docker/etc/nginx/vhost.common.d/10-general.conf
RUN echo "gzip_min_length 1000;" >> /opt/docker/etc/nginx/vhost.common.d/10-general.conf
RUN echo "gzip_static on;" >> /opt/docker/etc/nginx/vhost.common.d/10-general.conf
RUN echo "gunzip on;" >> /opt/docker/etc/nginx/vhost.common.d/10-general.conf

RUN sed -i "s|CipherString = DEFAULT@SECLEVEL=2|CipherString = DEFAULT@SECLEVEL=1 |g" /etc/ssl/openssl.cnf

COPY docker/entrypoints/97-run-composer.sh /opt/docker/provision/entrypoint.d/97-run-composer.sh
COPY docker/entrypoints/98-fix-permissions.sh /opt/docker/provision/entrypoint.d/98-fix-permissions.sh
COPY docker/entrypoints/97-run-npm.sh /opt/docker/provision/entrypoint.d/97-run-npm.sh

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - 
RUN apt-get install -y nodejs
RUN npm install -g yarn

COPY "docker/supervisor/laravel-queue-worker.conf" /opt/docker/etc/supervisor.d/laravel-queue-worker.conf

RUN mkdir -p /app/storage/logs
RUN touch /app/storage/logs/laravel.log

WORKDIR /app

RUN chown -R application:application /app