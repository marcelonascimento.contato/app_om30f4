<?php

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

beforeEach(function () {
    $this->base_endpoint = '/api/v1/search';
});

it('should return cep information successfully', function () {
    Http::fake([
        'https://viacep.com.br/*' => Http::response([
            'cep' => '01001-000',
            'logradouro' => 'Praça da Sé',
            'bairro' => 'Sé',
            'localidade' => 'São Paulo',
            'uf' => 'SP'
        ], 200)
    ]);

    $response = $this->get("{$this->base_endpoint}?zip_code=01001000");

    $response->assertStatus(Response::HTTP_FOUND)
        ->assertJsonStructure(['data' => [
            'zip_code',
            'address',
            'neighborhood',
            'city',
            'state'
        ]]);

    $data = json_decode($response->getContent(), true)['data'];
    expect($data['zip_code'])->toBe('01001-000');
    expect($data['address'])->toBe('Praça da Sé');
    expect($data['neighborhood'])->toBe('Sé');
    expect($data['city'])->toBe('São Paulo');
    expect($data['state'])->toBe('SP');
});

it('should return an error when request failed', function () {
    Http::fake([
        'https://viacep.com.br/*' => Http::response([], 500)
    ]);

    $response = $this->get("{$this->base_endpoint}?zip_code=01001000");

    $response->assertStatus(Response::HTTP_INTERNAL_SERVER_ERROR)
        ->assertJson(['message' => 'Failed to retrieve CEP information']);
});

it('should return cached cep information successfully', function () {
    Http::fake([
        'https://viacep.com.br/*' => Http::response([
            'cep' => '01001-000',
            'logradouro' => 'Praça da Sé',
            'bairro' => 'Sé',
            'localidade' => 'São Paulo',
            'uf' => 'SP'
        ], 200)
    ]);

    Cache::shouldReceive('remember')
        ->once()
        ->andReturn([
            'zip_code' => '01001-000',
            'address' => 'Praça da Sé',
            'neighborhood' => 'Sé',
            'city' => 'São Paulo',
            'state' => 'SP'
        ]);

    $response = $this->get("{$this->base_endpoint}/?zip_code=01001000");

    $response->assertStatus(Response::HTTP_FOUND)
        ->assertJsonStructure(['data' => [
            'zip_code',
            'address',
            'neighborhood',
            'city',
            'state'
        ]]);

    $data = json_decode($response->getContent(), true)['data'];
    expect($data['zip_code'])->toBe('01001-000');
    expect($data['address'])->toBe('Praça da Sé');
    expect($data['neighborhood'])->toBe('Sé');
    expect($data['city'])->toBe('São Paulo');
    expect($data['state'])->toBe('SP');
});
