<?php

use App\Jobs\PatientImportJob;
use App\Models\Patient;
use App\Models\PatientAddress;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

beforeEach(function () {
    $this->patientFactory = Patient::factory();
    $this->addressFactory = PatientAddress::factory();

    $this->validCNS = 294230287350001;

    $this->patientStructure = [
        "id",
        "full_name",
        "mother_name",
        "birth_date",
        "cpf",
        "cns",
        "created_at",
        "updated_at",
        "photo_url",
        "address",
    ];

    $this->base_endpoint = '/api/v1/patient';
});

it('should return a 200 status code when accessing the index route', function () {
    $patientCount = 3;

    $patients = Patient::factory($patientCount)->create();

    $response = $this->get($this->base_endpoint);

    $response->assertOk()
        ->assertJsonStructure([
            'data' => ['*' => $this->patientStructure]
        ])
        ->assertJsonCount($patientCount, 'data');

    foreach ($patients as $patient) {
        $response->assertJsonFragment([
            'id' => $patient->id,
            'full_name' => $patient->full_name,
            'mother_name' => $patient->mother_name,
            'birth_date' => $patient->birth_date,
            'cpf' => $patient->cpf,
            'cns' => $patient->cns,
            'created_at' => $patient->created_at,
            'updated_at' => $patient->updated_at,
            'photo_url' => $patient->photo_url
        ], 'data');
    }
});

it('should filter by cpf when accessing the index route with cpf param', function () {
    $patientCount = 3;

    $patients = Patient::factory($patientCount)->create();

    $firstPatient = $patients[0];

    $response = $this->get("{$this->base_endpoint}?cpf={$firstPatient->cpf}");

    $response = $response->assertOk()
        ->assertJsonStructure([
            'data' => ['*' => $this->patientStructure]
        ])
        ->assertJsonCount(1, 'data');

    $responseData = $response->json('data');

    expect($responseData[0]['id'])->toBe($firstPatient->id);
    expect($responseData[0]['cpf'])->toBe($firstPatient->cpf);
});

it('should filter by name when accessing the index route with name param', function () {
    $patientCount = 3;

    $patients = Patient::factory($patientCount)->create();

    $firstPatient = $patients[0];

    $response = $this->get("{$this->base_endpoint}?full_name={$firstPatient->full_name}");

    $response = $response->assertOk()
        ->assertJsonStructure([
            'data' => ['*' => $this->patientStructure]
        ])
        ->assertJsonCount(1, 'data');

    $responseData = $response->json('data');

    expect($responseData[0]['id'])->toBe($firstPatient->id);
    expect($responseData[0]['full_name'])->toBe($firstPatient->full_name);
});

it('should return a 400 status code when submitting a invalid cpf to the index', function () {
    $response = $this->get("{$this->base_endpoint}?cpf=9999");

    $response = $response
        ->assertStatus(422);
});

it('should return a 400 status code when submitting a invalid data to the store route', function () {
    $response = $this->post($this->base_endpoint, []);
    $response->assertStatus(400);
});

it('should return a 400 status code when submitting a invalid cpf to the store route', function () {
    $postData = $this->patientFactory->make(['cpf' => 99999999999])->toArray();
    $postData['cns'] = (string) $this->validCNS;

    $request = $this->post($this->base_endpoint, $postData)
        ->assertStatus(400);

    expect($request->json('message'))->toBe('The cpf field is not a valid CPF number');
});

it('should return a 400 status code when submitting a invalid cns to the store route', function () {
    $postData = $this->patientFactory->make(['cns' => '99999999999'])->toArray();

    $request = $this->post($this->base_endpoint, $postData)
        ->assertStatus(400);

    expect($request->json('message'))->toBe('The cns field is not a valid CNS number');
});

it('should return a 400 status code when submitting a partial address to the store route', function () {
    $addressFactory = $this->addressFactory->make()->toArray();
    $patientFactory = $this->patientFactory->make()->toArray();

    $postData = $patientFactory;
    $postData['patient_address'] = $addressFactory;
    $postData['cns'] = (string) $this->validCNS;

    unset($postData['patient_address']['zip_code']);

    $this->post($this->base_endpoint, $postData)
        ->assertStatus(400);
});

it('should create a new patient', function () {
    Storage::fake('public');

    $postData = $this->patientFactory->make()->toArray();
    $postData['cns'] = (string) $this->validCNS;

    $file = new UploadedFile(storage_path('testing/photo.jpg'), 'photo.jpg', 'jpg', null, true);

    $postData['photo'] = $file;

    $this->post($this->base_endpoint, $postData)
        ->assertStatus(201);

    $postData['birth_date'] = Carbon\Carbon::parse($postData['birth_date']);

    unset($postData['photo_url'], $postData['photo']);

    $this->assertDatabaseHas('patients', $postData);

    Storage::fake('public');
});

it('should create a new patient with address', function () {
    $addressFactory = $this->addressFactory->make()->toArray();
    $patientFactory = $this->patientFactory->make()->toArray();

    $postData = $patientFactory;
    $postData['patient_address'] = $addressFactory;
    $postData['cns'] = (string) $this->validCNS;

    $this->post($this->base_endpoint, $postData)
        ->assertStatus(201);

    $this->assertDatabaseHas('patients', [
        'cpf' => $postData['cpf'],
        'cns' => $postData['cns']
    ]);

    $this->assertDatabaseHas('patient_addresses', Arr::except($addressFactory, ['id', 'patient_id', 'created_at', 'updated_at']));
});


it('should return a specific patient', function () {
    $patient = $this->patientFactory->create();

    $response = $this->get($this->base_endpoint . '/' . $patient->id);

    $response->assertStatus(200);

    $dataResponse = $response->json('data');

    expect($dataResponse['id'])->toBe($patient->id);
    expect($dataResponse['full_name'])->toBe($patient->full_name);
});

it('should return a 404 error if patient does not exist', function () {
    $response = $this->get($this->base_endpoint . '/9999')
        ->assertStatus(404);

    expect($response->json('message'))->toBe("Patient not found.");
});

it('should delete a specific patient', function () {
    $patient = Patient::factory()->create();

    $this->delete("{$this->base_endpoint}/{$patient->id}")
        ->assertStatus(204);

    $this->assertDatabaseMissing('patients', ['id' => $patient->id]);
});

it('should return a 404 error when try to delete a patient that does not exist', function () {
    $response = $this->delete("{$this->base_endpoint}/99999");

    $response->assertStatus(404);
});

it('should return a 500 status code when try to delete a patient and internal server error happens', function () {
    \Schema::dropIfExists('patients');

    $this->delete("{$this->base_endpoint}/99999")
        ->assertStatus(500);
});

it('should return a 500 status code when try import patient and internal server error happens', function () {
    config(['queue.default' => 'sync']);

    $file = UploadedFile::fake()->create('file.csv');

    $this->post("/api/v1/import/patient", ['file' => $file])
        ->assertStatus(500);
});

it('should import a patients when try import patient using valid file', function () {
    config(['queue.default' => 'sync']);

    $file = new UploadedFile(storage_path('testing/import.csv'), 'import.csv', 'csv', null, true);

    $this->post("/api/v1/import/patient", ['file' => $file])
        ->assertOk();

    $this->assertDatabaseHas('patients', ['cpf' => 22657469086]);
    $this->assertDatabaseHas('patients', ['cpf' => 63454344000]);
});

it('should import a patients and update when try import patient using valid file', function () {
    config(['queue.default' => 'sync']);

    $file = new UploadedFile(storage_path('testing/import.csv'), 'import.csv', 'csv', null, true);

    $this->post("/api/v1/import/patient", ['file' => $file])
        ->assertOk();


    $this->post("/api/v1/import/patient", ['file' => $file])
        ->assertOk();

    $this->assertDatabaseHas('patients', ['cpf' => 22657469086]);
    $this->assertDatabaseHas('patients', ['cpf' => 63454344000]);
});

it('should update a specific patient', function () {
    Storage::fake('public');

    $patient = $this->patientFactory->create();

    $newName = 'Patient lorem ipsum';

    $file = new UploadedFile(storage_path('testing/photo.jpg'), 'photo.jpg', 'jpg', null, true);

    $response = $this->put("{$this->base_endpoint}/{$patient->id}", [
        'full_name' => $newName,
        'photo' => $file
    ]);

    $response->assertStatus(200);

    $response->assertJson([
        'id' => $patient->id,
        'full_name' => $newName,
    ]);

    $this->assertDatabaseHas('patients', [
        'id' => $patient->id,
        'full_name' => $newName
    ]);

    Storage::fake('public');
});

it('should update a specific patient when has address', function () {
    $patientAddress = $this->addressFactory->make()->toArray();

    $patient = $this->patientFactory->create();

    $newName = 'foo bar patient';

    unset($patientAddress['patient_id']);

    $postData = [
        'full_name' => $newName,
        'patient_address' => $patientAddress
    ];

    $response = $this->put("{$this->base_endpoint}/{$patient->id}", $postData);

    $response->assertStatus(200);

    $response->assertJson([
        'id' => $patient->id,
        'full_name' => $newName,
        'address' => [
            'number' => $patientAddress['number']
        ]
    ]);

    $this->assertDatabaseHas('patients', [
        'id' => $patient->id,
        'full_name' => $newName
    ]);
});

it('should return a 404 error when try to update a patient that does not exist', function () {
    $request = $this->PUT("{$this->base_endpoint}/9999", [])
        ->assertStatus(404);

    expect($request->json('message'))->toBe('Patient not found.');
});
