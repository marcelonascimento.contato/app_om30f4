<?php

namespace App\Repositories;

use App\Interfaces\CrudRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CrudRepository implements CrudRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes = []): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param $id
     *
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param $id
     * @param $with
     * @param $without
     *
     * @return Builder|array<Builder>|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function get($id, $with = [], $without = [])
    {
        return $this->model->when($without, function ($query, $without) {
            $query->without($without);
        })->when($with, function ($query, $with) {
            $query->withOnly($with);
        })->findOrFail($id);
    }

    /**
     * @param $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->model->whereId($id)->update($data);
    }

    /**
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @return mixed
     */
    public function deleteAll()
    {
        return $this->model->query()->delete();
    }

    /**
     * @param array $with
     * @param array $without
     *
     * @return \Illuminate\Database\Eloquent\Collection|array<\Illuminate\Database\Eloquent\Model>
     */
    public function getAll(array $with = [], array $without = [])
    {
        return $this->model->when($without, function ($query, $without) {
            $query->without($without);
        })->when($with, function ($query, $with) {
            $query->withOnly($with);
        })->get();
    }

    /**
     * @param array $identifiers
     * @param array $data
     *
     * @return mixed
     */
    public function updateOrCreate(array $identifiers, array $data)
    {
        return $this->model->updateOrCreate($identifiers, $data);
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @param string $index
     * @param array $columns
     * @param bool $strToLower
     *
     * @return array
     */
    public function getByColumnValues(string $attribute, mixed $value, string $index = 'id', array $columns = [], bool $strToLower = false): array
    {
        $dataList = ! empty($columns)
            ? $this->model->whereIn($attribute, $value)->get($columns)
            : $this->model->whereIn($attribute, $value)->get();

        $results = [];
        if (! empty($dataList)) {
            foreach ($dataList->toArray() as $item) {
                if ($strToLower) {
                    $results[$item[$index]] = \array_map(fn ($value) => is_string($value) ? strtolower($value) : $value, $item);
                } else {
                    $results[$item[$index]] = $item;
                }
            }
        }

        return $results;
    }

    /**
     * It takes an array of attributes and returns a query builder object.
     *
     * @param array attributes An array of attributes to search for.
     *
     * @return A query builder object.
     */
    public function where(array $attributes)
    {
        $result = $this->model;

        foreach ($attributes as $column => $value) {
            $result = $this->model->where($column, $value);
        }

        return $result;
    }
}
