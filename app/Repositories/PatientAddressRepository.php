<?php

namespace App\Repositories;

use App\Interfaces\PatientAddressRepositoryInterface;
use App\Models\PatientAddress;

class PatientAddressRepository extends CrudRepository implements PatientAddressRepositoryInterface
{
    public function __construct(PatientAddress $model)
    {
        parent::__construct($model);
    }
}
