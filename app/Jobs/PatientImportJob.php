<?php

namespace App\Jobs;

use App\Services\PatientImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class PatientImportJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of unhandled exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 2;

    /**
     * @var int
     */
    public $timeout = 60 * 3;

    protected $importer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private mixed $filePath,
    ) {
        //
    }

    public function getKey(): string
    {
        return hash_file('sha256', storage_path('app/public/' . $this->filePath));
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware()
    {
        return [
            (new WithoutOverlapping($this->getKey()))
                ->releaseAfter(60)
                ->expireAfter($this->timeout)
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importer = resolve(PatientImportService::class);

        $publicPath = storage_path('app/public/' . $this->filePath);

        $collection = $importer->xlsxToCollection($publicPath);

        $collection = $importer->prepareToValidation($collection);

        $importer->validateData($collection);

        if ($importer->hasErrors()) {
            $errors = $importer->getErrorsFormated();

            Log::error('Error on import patients', [
                'errors' => $errors,
                'collection' => $collection
            ]);

            throw \Exception('Error on import patients');
        } else {
            try {
                $validData = $importer->getValidated();

                $data = $importer->prepareToImport($validData);

                $importer->import($data);
            } catch (\Throwable $th) {
                Log::error('Error on import patients', [
                    'error' => $th->getMessage(),
                    'collection' => $collection
                ]);

                throw \Exception('Error on import patients');
            }
        }
    }
}
