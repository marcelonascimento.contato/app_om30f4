<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if (method_exists($e, 'render') && $e->render($request)?->original) {
            $response = $e->render($request)?->original;
        } else {
            $response = ['message' => $e->getMessage()];
        }

        if (property_exists($e, 'response')) {
            $reflectionProp = new \ReflectionProperty($e, 'response');

            if (
                $reflectionProp->isPublic()
                && $e?->response
                && property_exists($e->response, 'original')
                && isset($e?->response?->original['errors'])
            ) {
                $response = array_merge($e->response->original, $response);
            }
        }

        if (isset($response['errors']) && $response['errors'] instanceof \Illuminate\Support\MessageBag) {
            $errors = [];
            foreach ($response['errors']->toArray() as $key => $messageArray) {
                $errors[] = implode(', ', $messageArray);
            }
            if ($e instanceof \Illuminate\Validation\ValidationException) {
                $response['message'] = \implode('. ', $errors);
            } else {
                $response['message'] = __($response['message']) . ' ' . \implode('. ', $errors);
            }
        }

        if (config('app.debug')) {
            $response['exception'] = $e::class; // Reflection might be better here
            $response['trace'] = $e->getTraceAsString();
        } elseif (isset($response['trace'])) {
            unset($response['trace']);
        }

        $status = 400;
        if ($this->isHttpException($e)) {
            $status = $e->getStatusCode();
        }

        return response()->json($response, $status);
    }
}
