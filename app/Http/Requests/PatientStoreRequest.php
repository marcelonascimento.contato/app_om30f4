<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'full_name' => ['required', 'string', 'max:255'],
            'mother_name' => ['required', 'string', 'max:255'],
            'birth_date' => ['required', 'date', 'before:today'],
            'cpf' => ['required', 'cpf', 'unique:patients'],
            'cns' => ['required', 'cns', 'unique:patients'],
            'patient_address' => ['filled', 'array'],
            'photo' => ['nullable', 'image', 'max:5120'],
        ];

        if ($this->has('patient_address')) {
            $rules['patient_address.zip_code'] = ['required', 'string', 'max:9'];
            $rules['patient_address.number'] = ['required', 'string', 'max:10'];
            $rules['patient_address.city'] = ['required', 'string', 'max:255'];
            $rules['patient_address.state'] = ['required', 'string', 'max:2'];
            $rules['patient_address.address'] = ['required', 'string', 'max:255'];
            $rules['patient_address.complement'] = ['nullable', 'string', 'max:255'];
            $rules['patient_address.neighborhood'] = ['required', 'string', 'max:255'];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'unique' => 'The :attribute already exists.',
            'cpf.cpf' => 'The :attribute field is not a valid CPF number',
            'cns.cns' => 'The :attribute field is not a valid CNS number'
        ];
    }
}
