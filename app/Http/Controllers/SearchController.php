<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{
    use ApiResponser;

    public function __invoke(SearchRequest $request)
    {
        $zipcode = $request->get('zip_code');

        $ttl = 60 * 60 * 24 * 7; // seven days

        $data = Cache::remember("cep_information:{$zipcode}", $ttl, function () use ($zipcode) {
            $response = Http::asJson()->timeout(120)->connectTimeout(30)
                ->withoutVerifying()
                ->get("https://viacep.com.br/ws/{$zipcode}/json/");

            if (!$response->ok()) {
                return [];
            };

            return $this->normalizeData($response->json());
        });

        if (empty($data)) {
            return $this->error(
                message: "Failed to retrieve CEP information",
                code: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->success(
            data: ['data' => $data],
            code: Response::HTTP_FOUND
        );
    }

    protected function normalizeData(array $data = []): array
    {
        if (empty($data)) {
            return [];
        }

        return [
            'zip_code' => $data['cep'] ?? null,
            'address' => $data['logradouro'] ?? null,
            'neighborhood' => $data['bairro'] ?? null,
            'city' => $data['localidade'] ?? null,
            'state' => $data['uf'] ?? null
        ];
    }
}
