<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatientImportRequest;
use App\Http\Requests\PatientIndexRequest;
use App\Http\Requests\PatientStoreRequest;
use App\Http\Requests\PatientUpdateRequest;
use App\Http\Resources\PatientResource;
use App\Interfaces\PatientAddressRepositoryInterface;
use App\Interfaces\PatientRepositoryInterface;
use App\Jobs\PatientImportJob;
use App\Services\PatientService;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PatientController extends Controller
{
    use ApiResponser;

    public const DEFAULT_PAGE_SIZE = 10;
    public const PATIENT_AVATAR_FOLDER = 'patients_avatar';

    /**
     * @param PatientRepositoryInterface $patientRepository
     * @param PatientAddressRepositoryInterface $patientAddressRepositoryInterface
     * @param PatientService $patientService
     */

    public function __construct(
        private PatientRepositoryInterface $patientRepository,
        private PatientAddressRepositoryInterface $patientAddressRepository,
        private PatientService $patientService
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\PatientIndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PatientIndexRequest $request)
    {
        $resource = $this->patientRepository
                        ->getModel()
                        ->filter($request->validated())
                        ->with('address')
                        ->paginate($request->validated()['page_size'] ?? self::DEFAULT_PAGE_SIZE);

        return PatientResource::collection($resource);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PatientStoreRequest $request
     * @return JsonResponse
     */
    public function store(PatientStoreRequest $request): JsonResponse
    {
        $params = $request->validated();

        try {
            DB::beginTransaction();

            $patient = $this->patientService->createPatient($params);

            DB::commit();

            return $this->success(
                data: $patient,
                code: Response::HTTP_CREATED
            );
        } catch (\Throwable $th) {
            DB::rollBack();

            $this->patientService->deletePatientPhoto();

            return $this->error(
                message: 'Unable to create patient. An error occurred while processing the request. Please try again later.',
                code: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $patient
     * @return JsonResource|JsonResponse
     */
    public function show(int $patientId): JsonResource|JsonResponse
    {
        $patient = $this->patientRepository->find($patientId);

        if (empty($patient)) {
            return $this->error(
                message: 'Patient not found.',
                code: Response::HTTP_NOT_FOUND
            );
        }

        return new PatientResource($patient->load('address'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(PatientUpdateRequest $request, int $patientId): JsonResponse
    {
        $patient = $this->patientRepository->find(id: $patientId);

        if (empty($patient)) {
            return $this->error(
                message: 'Patient not found.',
                code: Response::HTTP_NOT_FOUND
            );
        }

        DB::beginTransaction();

        $patient = $this->patientService->updatePatient(
            patient:$patient,
            requestData: $request->validated()
        );

        DB::commit();

        return $this->success($patient->load('address'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $patient
     * @return JsonResponse
     */
    public function destroy(int $patientId): JsonResponse
    {
        try {
            DB::beginTransaction();

            $patient = $this->patientRepository->find($patientId);

            if (empty($patient)) {
                return $this->error(
                    message: 'Patient not found.',
                    code: Response::HTTP_NOT_FOUND
                );
            }

            $this->patientRepository->delete($patientId);

            DB::commit();

            return $this->success(
                data: '',
                code: Response::HTTP_NO_CONTENT
            );
        } catch (\Throwable $th) {
            DB::rollBack();

            return $this->error(
                message: 'Unable to delete patient. An error occurred while processing the request. Please try again later.',
                code: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function import(PatientImportRequest $request): JsonResponse
    {
        try {
            $public_path = Storage::disk('public')->put('patient_import', $request['file']);

            PatientImportJob::dispatch($public_path);

            return $this->success(
                data: ['message' => 'Patient data import request received. The import process is running in the background.'],
                code: Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            return $this->error(
                message: 'An error occurred while processing the patient import request. Please try again later.',
                code: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
