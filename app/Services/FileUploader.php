<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploader
{
    /**
     * The name of the directory where the file will be stored.
     *
     * @var string
     */
    protected $type;

    /**
     * The uploaded file instance.
     *
     * @var UploadedFile
     */
    protected $file;

    /**
     * The filename of the uploaded file.
     *
     * @var string
     */
    protected $filename;

    /**
     * The file path of the uploaded file.
     *
     * @var string
     */
    protected $filePath;


    /**
     * Set the uploaded file instance.
     *
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file): self
    {
        $this->file = $file;
        return $this;
    }

    /**
     * Set the type (directory name) where the file will be stored.
     *
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Set the filename
     *
     * @param string $type
     * @return $this
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Set the filePath
     *
     * @param string $type
     * @return $this
     */
    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get the type (directory name) where the file will be stored.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get the uploaded file instance.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get the filename.
     *
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Get the filePath.
     *
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * Get a randomly generated filename for the uploaded file.
     *
     * @return string
     */
    public function generateRandomFilename(): string
    {
        $extension = $this->file->getClientOriginalExtension();

        return \Str::random(40) . '.' . $extension;
    }

    /**
     * Store the uploaded file in the appropriate directory.
     *
     * @return string|null
     */
    public function storeFile(): ?string
    {
        if (!$this->file) {
            return null;
        }

        $path = Storage::disk('public')->putFileAs($this->getType(), $this->getFile(), $this->getFilename());

        $this->setFilePath($path);

        return $path;
    }

    /**
     * Delete the specified file from storage.
     *
     * @return bool
     */
    public function deleteFile(): bool
    {
        if (!$this->getFilePath()) {
            return true;
        }

        return Storage::disk('public')->delete($this->getFilePath());
    }

    public function setRandomName(): self
    {
        $this->setFilename($this->generateRandomFilename());

        return $this;
    }
}
