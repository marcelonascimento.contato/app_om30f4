<?php

namespace App\Services;

use App\Models\Patient;
use App\Repositories\PatientAddressRepository;
use App\Repositories\PatientRepository;
use Illuminate\Http\UploadedFile;

class PatientService
{
    // Define a constant variable to store the folder name where patient avatars will be stored.
    public const PATIENT_AVATAR_FOLDER = 'patient_avatar';

    /**
     * @param PatientRepository $patientRepository
     * @param PatientAddressRepository $patientAddressRepository
     * @param FileUploader $fileUploader
     */

    public function __construct(
        private PatientRepository $patientRepository,
        private PatientAddressRepository $patientAddressRepository,
        private FileUploader $fileUploader
    ) {
    }

    /**
     * Method to create a new patient record in the database
     * @param array $requestData An array with the patient data and address data
     * @return Patient|null Returns the patient object created or null if it failed to create
     */
    public function createPatient(array $requestData): ?Patient
    {
        $addressData = $requestData['patient_address'] ?? [];

        unset($requestData['patient_address']);

        $patientData = $requestData;

        unset($patientData['photo']);

        $patient = $this->patientRepository->create($patientData);

        if ($addressData && $patient) {
            $addressData['patient_id'] = $patient->id;
            $this->patientAddressRepository->create($addressData);
            $patient->load('address');
        }

        if (isset($requestData['photo']) && $requestData['photo'] instanceof UploadedFile) {
            $photoPath = $this->storePatientPhoto($requestData['photo']);

            $patient->photo = $photoPath;

            $patient->save();
        }

        return $patient;
    }

    /**
     * It updates a patient's data and address, and if a new photo is uploaded, it stores it and updates
     * the patient's photo path
     *
     * @param patient The patient instance or the patient ID.
     * @param requestData This is the data that is passed to the controller from the form.
     *
     * @return ?Patient The patient object.
     */
    public function updatePatient($patient, $requestData): ?Patient
    {
        if (! $patient instanceof Patient) {
            $patient = $this->patientRepository->find(id: $patient);
        }

        $addressData = $requestData['patient_address'] ?? [];

        unset($requestData['patient_address'], $requestData['_method']);

        $patientData = $requestData;

        unset($patientData['photo']);

        $this->patientRepository->update(
            id: $patient->id,
            data: $patientData
        );

        if (! empty($addressData)) {
            if ($patient->address) {
                $this->patientAddressRepository->update(
                    id: $patient->address->id,
                    data: $addressData
                );
            } else {
                $addressData['patient_id'] = $patient->id;

                $this->patientAddressRepository->create($addressData);
            }
        }

        if (isset($requestData['photo']) && $requestData['photo'] instanceof UploadedFile) {
            $photoPath = $this->storePatientPhoto(
                file: $requestData['photo'],
                patient: $patient
            );

            if ($patient->photo != $photoPath) {
                $patient->photo = $photoPath;

                $patient->save();
            }
        }

        return $patient->refresh();
    }

    /**
     * Method to store a patient's photo using the FileUploader class
     * @param UploadedFile $file The photo file to be stored
     * @param ?Patient $patient
     * @return string Returns the path of the stored photo
     */
    protected function storePatientPhoto(UploadedFile $file, $patient = null): string
    {
        $this->fileUploader->setType(self::PATIENT_AVATAR_FOLDER)
                               ->setFile($file);

        if ($patient?->photo) {
            $photoHandler = explode('/', $patient->photo);

            $filename = end($photoHandler);

            $this->fileUploader->setFilename($filename);
        } else {
            $this->fileUploader->setRandomName();
        }

        return $this->fileUploader->storeFile();
    }

    /**
     * Method to delete a patient's photo using the FileUploader class
     */
    public function deletePatientPhoto(): void
    {
        $this->fileUploader->deleteFile();
    }
}
