<?php

namespace App\Services;

use App\Interfaces\PatientAddressRepositoryInterface;
use App\Interfaces\PatientRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;

class PatientImportService
{
    protected array $errors = [];

    protected array $validData = [];

    public const HEADER_TRANSLATION = [
        'nome' => 'full_name',
        'nome mae' => 'mother_name',
        'data de nascimento' => 'birth_date',
        'cpf' => 'cpf',
        'cns' => 'cns',
        'endereco' => 'address',
        'numero' => 'number',
        'bairro' => 'neighborhood',
        'cidade' => 'city',
        'estado' => 'state',
        'complemento' => 'complement',
        'cep' => 'zip_code'
    ];

    public array $addressFields = [
        "zip_code",
        "number",
        "city",
        "state",
        "address",
        "complement",
        "neighborhood",
    ];

    protected array $singleRules = [
        'full_name' => ['string', 'max:255'],
        'mother_name' => ['string', 'max:255'],
        'birth_date' => ['date', 'before:today'],
        'cpf' => ['cpf'],
        'cns' => ['cns'],
        'zip_code' => ['string', 'max:9'],
        'number' => ['string', 'max:10'],
        'city' => ['string', 'max:255'],
        'state' => ['string', 'max:2'],
        'address' => ['string', 'max:255'],
        'complement' => ['nullable', 'string', 'max:255'],
        'neighborhood' => ['string', 'max:255'],
    ];

    public function __construct(
        private PatientRepositoryInterface $patientRepository,
        private PatientAddressRepositoryInterface $patientAddressRepository
    ) {
    }

    public function xlsxToCollection($public_path, $callback = null)
    {
        if (is_null($callback)) {
            $callback = function ($line) {
                $new_line = [];
                foreach ($line as $key => $value) {
                    if (isset(self::HEADER_TRANSLATION[strtolower($key)])) {
                        $new_line[self::HEADER_TRANSLATION[strtolower($key)]] = $value;
                    }
                }

                return $new_line;
            };
        }

        return (new FastExcel())->import($public_path, $callback);
    }

    public function prepareToValidation(Collection $collection): Collection
    {
        $items = $collection->all();
        $handler = [];

        foreach ($items as $fields) {
            foreach ($fields as $field => $value) {
                $handler[$field][] = $value;
            }
        }

        return new Collection($handler);
    }

    public function validateData(Collection $data): bool
    {
        $rules = [];

        foreach ($this->singleRules as $type => $rule) {
            $rules[$type] = ['required', 'array'];
            $rules["{$type}.*"] = $rule;
        }

        $dataArray = $data->toArray();

        $validator = Validator::make($dataArray, $rules);

        if ($validator->fails()) {
            $this->errors = $validator->errors()->all();

            return false;
        }

        $this->validData = $validator->validated();

        return true;
    }

    public function prepareToImport(array $data): Collection
    {
        $items = [];

        foreach ($data as $field => $fields) {
            $count = 0;

            foreach ($fields as $value) {
                $items[$count][$field] = $value;

                $count++;
            }
        }

        return new Collection($items);
    }


    public function import($data)
    {
        foreach ($data as $item) {
            DB::beginTransaction();

            try {
                $patientData = Arr::except($item, $this->addressFields);

                $patient = $this->patientRepository->updateOrCreate([
                    "cpf" => $patientData['cpf']
                ], $patientData);

                $addressData = array_filter(Arr::only($item, $this->addressFields));

                if ($patient->address) {
                    $this->patientAddressRepository->update($patient->id, $addressData);
                }

                if (!$patient->address && $this->checkIfAllKeysExists(array_keys($addressData), array_values($this->addressFields))) {
                    $addressData['patient_id'] = $patient->id;

                    $this->patientAddressRepository->create($addressData);
                }

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();

                throw $th;
            }
        }
    }

    public function hasErrors(): bool
    {
        return ! empty($this->errors);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getErrorsFormated(): array
    {
        $errors = $this->getErrors();
        $formated_Errors = [];
        foreach ($errors as $key => $error) {
            $formated_Errors[] = ['error' => $error];
        }

        return $formated_Errors;
    }

    public function getValidated(): array
    {
        return $this->validData;
    }

    public function checkIfAllKeysExists($array1, $array2)
    {
        return !array_diff_key($array1, $array2) && !array_diff_key($array2, $array1);
    }
}
