<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

use function implode;

trait ApiResponser
{
    /**
     * Return a success JSON response.
     *
     * @param array|string $data
     * @param int $code
     *
     * @return JsonResponse
     */
    protected function success($data, int $code = null): JsonResponse
    {
        return response()->json($data, $code ?? Response::HTTP_OK);
    }

    /**
     * Return an error JSON response.
     *
     * @param string|null $message
     * @param int $code
     * @param null $data
     *
     * @return JsonResponse
     */
    protected function error(?string $message, int $code, $data = null): JsonResponse
    {
        $response = response();

        return $response->json([
            'message' => $message,
            'data' => $data,
        ], $code ?? Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param array $messages
     *
     * @return string
     */
    protected function mergeMessages(array $messages): string
    {
        return implode(".\n", $messages);
    }
}
