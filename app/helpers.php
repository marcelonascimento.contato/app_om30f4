<?php

namespace App\Helpers;

if (! function_exists('removeNonDigits')) {
    /**
     * It removes all non-digit characters from a string
     *
     * @param string string The string to be modified.
     *
     * @return string The string with all non-digits removed.
     */
    function removeNonDigits(string $string): string
    {
        return preg_replace('/\D/', '', $string);
    }
}
