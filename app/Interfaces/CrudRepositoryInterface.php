<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface CrudRepositoryInterface
{
    public function get($id, array $with = [], array $without = []);

    public function delete($id);

    public function deleteAll();

    public function update($id, array $data);

    public function getAll(array $with = [], array $without = []);

    public function updateOrCreate(array $identifiers, array $data);

    public function getByColumnValues(string $attribute, mixed $value, string $index = 'id', array $columns = [], bool $strToLower = false): array;

    public function where(array $attributes);

    public function getModel(): Model;

    public function create(array $attributes = []): Model;

    public function find($id): ?Model;
}
