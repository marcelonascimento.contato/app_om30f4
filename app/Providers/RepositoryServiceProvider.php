<?php

namespace App\Providers;

use App\Interfaces\CrudRepositoryInterface;
use App\Interfaces\PatientAddressRepositoryInterface;
use App\Interfaces\PatientRepositoryInterface;
use App\Repositories\CrudRepository;
use App\Repositories\PatientAddressRepository;
use App\Repositories\PatientRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CrudRepositoryInterface::class, CrudRepository::class);
        $this->app->bind(PatientRepositoryInterface::class, PatientRepository::class);
        $this->app->bind(PatientAddressRepositoryInterface::class, PatientAddressRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
