<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use function App\Helpers\removeNonDigits;

class PatientAddress extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_id',
        'zip_code',
        'address',
        'number',
        'complement',
        'neighborhood',
        'city',
        'state',
    ];

    /**
     * Interact with the user's CPF.
     */
    protected function zipCode(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => removeNonDigits($value),
            set: fn (string $value) => removeNonDigits($value)
        );
    }

    /**
     * Get the patient that owns the address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }
}
