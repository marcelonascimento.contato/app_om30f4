<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use EloquentFilter\Filterable;

use function App\Helpers\removeNonDigits;

class Patient extends Model
{
    use HasFactory;
    use Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'photo',
        'full_name',
        'mother_name',
        'birth_date',
        'cpf',
        'cns'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birth_date' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'photo_url',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'photo',
    ];

    /**
     * Interact with the user's CPF.
     */
    protected function cpf(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => removeNonDigits($value),
            set: fn (string $value) => removeNonDigits($value)
        );
    }

    protected function cns(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => removeNonDigits($value),
            set: fn (string $value) => removeNonDigits($value)
        );
    }

    /**
     * Get the patient address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address(): HasOne
    {
        return $this->hasOne(PatientAddress::class);
    }

    public function getPhotoUrlAttribute()
    {
        if ($this->photo) {
            return Storage::disk('public')->url($this->photo);
        }

        return null;
    }

    public static function boot(): void
    {
        parent::boot();

        self::deleted(function ($patient) {
            if (!empty($patient->photo)) {
                $photo = $patient->photo;

                $photoPath = public_path("storage/{$photo}");

                if (File::isFile($photoPath)) {
                    File::delete($photoPath);
                }
            }
        });
    }
}
