# Sobre o projeto

## Introdução

Este projeto consiste em uma API Rest em PHP desenvolvida com o framework Laravel 9. A versão do PHP utilizada é a 8.

Além disso, a coleção de endpoints está disponível em endpoints.json para uso no Insomnia.

### O Desafio

Requisitos

A API deve permitir a realização das seguintes ações:
- Criar um novo paciente
- Listar todos os pacientes
- Filtrar pacientes por CPF e nome
- Atualizar um paciente existente
- Deletar um paciente existente
- Exibir um paciente específico
- Buscar CEP viaCEP (com cache no Redis)

Commits

Cada commit deve conter uma descrição clara e detalhada das implementações e entregas realizadas.

## A solução

#### Como rodar o projeto

**Antes de seguir os passos abaixo tenha certeza que o docker e docker-compose estão instalados na maquina. Para rodar este projeto:**

1\. O arquivo de configurações é o \.env (Já está configurado)\.

2\. Faça o build dos containers \, o container **app-backend** vai usar a porta 80 e o container **app-database** irá usar a porta 3306, certifique-se que essas portas estejam livres antes de continuar. Se seu usuário não estiver incluido no grupo de permissões do **docker e docker-compose**  será necessário executar os comandos como administrador (sudo)

```
docker-compose up -d
```

3\. Agora basta rodar os seguintes scripts na pasta do projeto.

```
cp .env.example .env
docker exec $(docker ps -aqf "name=app-backend") composer install
docker exec $(docker ps -aqf "name=app-backend") php artisan migrate --seed --force
docker exec $(docker ps -aqf "name=app-backend") chmod -R 777 storage bootstrap/cache
```

4\. Agora você pode acessar aplicação em [localhost ou clique aqui!](http://localhost)

5\. Certifique-se de que todas as funcionalidades estão funcionando corretamente executando o código abaixo (~ 90% code coverage).
```
docker exec $(docker ps -aqf "name=app-backend") vendor/bin/pest
```