<?php

namespace Database\Factories;

use Carbon\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Patient>
 */
class PatientFactory extends FakerPTBR
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->name(),
            'mother_name' => $this->faker->name('female'),
            'birth_date' => $this->faker->dateTimeBetween(Carbon::now()->subYear(50), Carbon::now()->subYear(10)),
            'cpf' => $this->faker->unique()->cpf(),
            'cns' => $this->faker->numerify('###############'),
            'photo' => \Str::random(12).'.png'
        ];
    }
}
