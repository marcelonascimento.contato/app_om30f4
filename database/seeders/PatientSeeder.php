<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Patient;
use App\Models\PatientAddress;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::factory()
            // ->has(PatientAddress::factory(), 'address')
            ->create()
            ->each(function ($patient) {
                $address = PatientAddress::factory()->create(['patient_id' => $patient->id]);

                $patient->address()->save($address);
            });
    }
}
