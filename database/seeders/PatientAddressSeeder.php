<?php

namespace Database\Seeders;

use App\Models\PatientAddress;
use Illuminate\Database\Seeder;

class PatientAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PatientAddress::factory()->count(1)->create();
    }
}
